package cn.tang.controller;


import cn.tang.entiry.DataUpdate;
import cn.tang.entiry.SelcetDataPlatformAndType;
import cn.tang.entiry.SelectData;
import cn.tang.entiry.ConditionSelectData;
import cn.tang.service.DataService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping(value = "/dataController")
public class DataController {
    @Resource
    DataService dataService;

    /**
     * 查询所有数据
     * @param data
     * @return
     */
    @RequestMapping(value = "/selectData")
    @ResponseBody
    public List<SelectData> selectData(SelcetDataPlatformAndType data){
       return dataService.selectData(data);
    }

    /**
     * 修改单条数据
     * @param data
     */
    @RequestMapping(value = "/updateData")
    @ResponseBody
    public void updateData(DataUpdate data){
         dataService.updateData(data);
    }
    /**
     * 条件查询(电话号码)
     * @param data
     */
    @RequestMapping(value = "/conditionSelectDataPhone")
    @ResponseBody
    public List<SelectData> conditionSelectDataPhone(ConditionSelectData data){
        String phone=data.getPhone();
        data.setPhone("%"+phone+"%");
       return dataService.conditionSelectDataPhone(data);
    }
    /**
     * 条件查询(用户名)
     * @param data
     */
    @RequestMapping(value = "/conditionSelectDataName")
    @ResponseBody
    public List<SelectData> conditionSelectDataName(ConditionSelectData data){
        String name=data.getUname();
        data.setUname("%"+name+"%");
        return dataService.conditionSelectDataName(data);
    }
    /**
     * 条件查询(时间)
     * @param data
     */
    @RequestMapping(value = "/conditionSelectDataTime")
    @ResponseBody
    public List<SelectData> conditionSelectDataTime(ConditionSelectData data){
        return dataService.conditionSelectDataTime(data);
    }
}
